// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "SlateBasics.h"
#include "ShotgunPlugStyle.h"

class FShotgunPlugCommands : public TCommands<FShotgunPlugCommands>
{
public:

	FShotgunPlugCommands()
		: TCommands<FShotgunPlugCommands>(TEXT("ShotgunPlug"), NSLOCTEXT("Contexts", "ShotgunPlug", "ShotgunPlug Plugin"), NAME_None, FShotgunPlugStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > SwapActorsLocationCommandInfo;
	TSharedPtr< FUICommandInfo > InvertActorCommandInfo;
};
