
#pragma once

#include "Engine.h"
#include "ModuleManager.h"
#include<string>
#include <Python.h>
#include"ShotgunTools.h"
#include<vector>


/****************************************************************************************************************

Notes:
- Every entity in Shotgun has a unique ID.
- The C++ ShotgunEntity is the base class storing ID and Name. Name is used chiefly for debug.
- Other Shotgun C++ classes derive from ShotgunEntity, some are only for code-clarity eg ShotgunTaskTemplate,
  others have member-vars and functions of their own, eg ShotgunTask.
- We will always create an Asset in Shotgun with a predefined TaskTemplate. This creates the asset and at the same time
  assigns a typical series of pipeline tasks to it. Eg create MoonLander also creates attached Model, Rig, and Animate tasks  

Typical workflow:
LOAD:
- Plugin loads all ShotgunUsers and TaskTemplates for the project in OnWorldCreated()
- In the level, an actor with a ShotgunData component is already tracked by Shotgun. The component
  stores the unique ID assigned by Shotgun at the time of creation, and the original actor name.
- Each tracked actor asks for its Tasks via the Connection object which in turn calls the Python API.
- The resultant Python dictionary is converted into a C++ ShotgunTask object and cached in the Repo's master map, keyed by ID.
- Tasks know of their Asset, their PipelineStep (eg Animate) and their list of Assignees.

ASSIGN/CREATE:
- Right-click on a TRACKED asset brings up a cascading series of menus to allow user-assignment to one of its tasks.
- Right-click on a NON-TRACKED asset brings up a submenu of all of the possible TaskTemplates.
- Clicking on a TaskTemplate will create an asset in Shotgun and also attach a predefined series of tasks.
- Eg we may create the Hercules Shotgun asset and at the same time attach Model, Rig, Texture, Surface and Animate tasks.
- The Repo is then updated with these newly created tasks and a new ShotgunData component is attached to the actor.
- This actor is now known to be Shotgun-tracked and can have its tasks assigned to specific users through the cascading menus.

*****************************************************************************************************************/




// The cascading menus are built with nested maps. These typedefs help clarify which keys are being used 
typedef FString FPipelineStep;
typedef FString FTaskName;
typedef FString FUserName;
 
/**
 * At the end of the cascading menus we trigger an action assigning a ShotgunTask to a ShotgunUser
 * This payload is loaded into the nested maps and passed through the various menu-building delegates.
 * Eventually it is used as the argument for ShotgunUserTaskAssign()
 */
struct UserTaskPayload 
{
	std::shared_ptr<ShotgunUser> Assignee;
	std::shared_ptr<ShotgunTask> Task;
};

class FToolBarBuilder;
class FMenuBuilder;

class FShotgunPlugModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
	
	
private:
	
	// The plugin's main interface to Shotgun
	ShotgunConnection  Connection;

	// Handle for ShutdownModule() cleanup
	FDelegateHandle MenuExtenderDelegateHandle;

	// FUIActions are created on the fly as user navigates submenus. We cache them here to avoid duplication
	TMap<FString, TSharedRef<FUIAction>> TaskTemplateActionLookup;

	// FUIActions are created on the fly as user navigates submenus. We cache them here to avoid duplication
	TMap<FString, TSharedRef<FUIAction>> AssignTaskActionLookup;


	/**
	 * Delegate handling the initial Shotgun query, the filtering of those level-actors already tracked, and the loading of their task data.
	 * Most of the relevant Shotgun data is loaded here, converted to the C++ model and stored in the Repo's ShotgunEntity containers. 
	 * @param World - Editor world, automatically passed by delegate creation/binding.
	 * @param IVS - UWorld::InitializationValues, automatically passed by delegate creation/binding.
	 */
	void OnWorldCreated(UWorld* World, const UWorld::InitializationValues IVS);


	/**
	 * Examines all world actors for existence of ShotgunData component.
	 * @param World - Editor World
	 * @return The actors currently tracked by Shotgun
	 */
	TArray<AActor*> GetAllShotgunTrackedActors(UWorld* World) ;


	/**
	* Delegate to extend the right-click context menu in the editor viewport.
	* @param CommandList - Automatically passed by delegate creation/binding
	* @param SelectedActors - Actors clicked/selected in the editor viewport. Automatically passed by delegate creation/binding.
	* @return MenuExtender hooking into the main editor right-click viewport menu
	*/
	TSharedRef<FExtender> AddContextMenuExtension(const TSharedRef<FUICommandList> CommandList, TArray<AActor*> SelectedActors);



	/**
	 * Delegate adding right-click menu items based on number of actors selected.
	 * Here is where we load our Shotgun menu items as well as the placeholders for Invert and Swap
	 * @param MenuBuilder - Automatically passed by delegate creation/binding.
	 * @param SelectedActors - Actors clicked/selected in the editor viewport. Automatically passed by delegate creation/binding - see AddContextMenuExtension()
	*/
	void FillPluginMenuOptions(FMenuBuilder& MenuBuilder, TArray<AActor*> SelectedActors);


	/**
	 * Builds main data structure for cascading menus. Visually expands in the context menu as lists of PipelineStep -> TaskName -> Username
	 * Eg Animation->Walk_Forward->Josh
	 * @param AssetID - Shotgun unique ID.
	 * @return Nested maps for menu creation.
	 */
	TMap<FPipelineStep, TMap<FTaskName, TMap<FUserName, UserTaskPayload>>> GetTaskMapsForAsset(int AssetID);


	/**
	* The menu option exposed when the clicked actor is already tracked in Shotgun, ie the actor has an attached ShotgunData component.
	* Gets nested task maps and passes them on to the submenu delegates.
	* @param MenuBuilder
	* @param AssetID - passed from the actor's UShotgunData component 
	*/
	void BuildTaskAssignMenus(FMenuBuilder& MenuBuilder, int AssetID) ;


	/**
	* Delegate populating the first cascading submenu: PipelineStep - eg Animation, Model, Texture, Rig
	* @param MenuBuilder
	* @param PipelineSteps - Nested maps, see GetTaskMapsForAsset() for details of data model, see BuildTaskAssignMenus() for argument bind
	*/
	void FillPipelineStepSubMenu(FMenuBuilder& MenuBuilder, TMap<FPipelineStep, TMap<FTaskName, TMap<FUserName, UserTaskPayload>>> PipelineSteps);
	
	
	/**
	* Delegate populating the second cascading submenu: Taskname - eg Jump, Attack, Walk_Fwd
	* @param MenuBuilder
	* @param TaskNamesMap - Nested maps, see GetTaskMapsForAsset() for details of data model, see FillPipelineStepSubMenu() for argument bind
	*/
	void FillTaskNameSubMenu(FMenuBuilder& MenuBuilder, TMap<FTaskName, TMap<FUserName, UserTaskPayload>> TaskNamesMap);


	/**
	* Delegate populating the last submenu: Username. Clicking one of these will assign a task to that user in Shotgun.
	* @param MenuBuilder
	* @param TaskNamesMap - Nested maps, see GetTaskMapsForAsset() for details of data model, see FillPipelineStepSubMenu() for argument bind
	*/
	void FillUserNameSubMenu(FMenuBuilder& MenuBuilder, TMap<FUserName, UserTaskPayload> UsernameTaskMap);


	/**
	* Menu option exposed when the clicked actor is not currently tracked by Shotgun. Generates submenu of task templates.
	* A task template in Shotgun automatically creates a series of tasks for a given asset.
	* Eg a character asset may be given Model, Rig, Texture, Surface and Animation tasks.
	* The template is typically pre-defined by production and here the user chooses one of the existing templates.
	* This will also create the asset in Shotgun and the unique ID will be returned and added to the clicked actor by way
	* of a new ShotgunData component.
	* @param MenuBuilder 
	* @param SelectedActor - Right-clicked actor in the editor viewport
	*/
	void BuildTaskTemplateCreationMenus(FMenuBuilder & MenuBuilder, AActor* SelectedActor);


	/**
	* Delegate populating the Task Template sub-menu.
	* @param MenuBuilder
	* @param SelectedActor - Right-clicked actor in the editor viewport.
	*/
	void FillTaskTemplateSubMenu(class FMenuBuilder& MenuBuilder, AActor* SelectedActor);


	/**
	* Assigns a Task to a User in Shotgun.
	* Calls the ShotgunTools static library which in turn calls the Python helper scripts interacting with Shotgun itself.
	* @param Payload - Holds shared_ptrs to the ShotgunUser and the original ShotgunTask. 
	*/
	void ShotgunUserTaskAssign(UserTaskPayload Payload);


	/**
	* Creates an Asset in Shotgun and assigns a series of Tasks to it.
	* @param TaskTemplate - contains the ID needed to trigger the Task Template creation in Shotgun
	* @param SelectedActor - Right-clicked actor in the editor viewport
	*/
	void ShotgunTaskTemplateCreation(std::shared_ptr<ShotgunTaskTemplate> TaskTemplate, AActor* SelectedActor);


	/**
	*  Early test for menu building and delgate creation. Leave as placeholder for future development.
	*  @param SelectedActors - Actors selected in the editor viewport. Automatically passed by delegate creation/binding
	*/
	void InvertActor(TArray<AActor*> SelectedActors);


	/**
	*  Early test for menu building and delgate creation. Leave as placeholder for future development
	*  @param SelectedActors - Actors selected in the editor viewport. Automatically passed by delegate creation/binding
	*/
	void SwapTwoActorsPosition(TArray<AActor*> SelectedActors);

};