#pragma once

#include "Components/ActorComponent.h"
#include "ShotgunData.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOTGUNPLUG_API UShotgunData : public UActorComponent
{
	GENERATED_BODY()


public:	

	/**
	* The presence of this component in an actor signifies that it is tracked by Shotgun.
	* Typically created and attached on successful completion of FShotgunPlugModule::ShotgunTaskTemplateCreation()
	*
	*
	* This is the ID Shotgun assigns to a newly-created Asset. It will also be used as a key 
	* in the master map lookup in the ShotgunTools Repo.
	*/

	UPROPERTY(VisibleAnywhere)
	int ShotgunID;

	/**
	* The original name of actor for debug purposes. Remains constant once set by Shotgun
	*/

	UPROPERTY(VisibleAnywhere)
	FText ShotgunName;

	// Sets default values for this component's properties
	UShotgunData();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

		
	
};
