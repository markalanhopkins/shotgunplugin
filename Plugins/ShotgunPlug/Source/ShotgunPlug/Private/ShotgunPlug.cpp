#include "ShotgunPlugPrivatePCH.h"
#include "SlateBasics.h"
#include "SlateExtras.h"
#include "ShotgunPlugStyle.h"
#include "ShotgunPlugCommands.h"
#include "LevelEditor.h"


static const FName ShotgunPlugTabName("ShotgunPlug");
#define LOCTEXT_NAMESPACE "FShotgunModule"

// Convenience console logger
static void QuickLog(FString Message) 
{
	UE_LOG(LogTemp, Warning, TEXT("%s"), *Message)
}

void FShotgunPlugModule::StartupModule()
{
	FShotgunPlugStyle::Initialize();
	FShotgunPlugStyle::ReloadTextures();
	FShotgunPlugCommands::Register();
	FLevelEditorModule& LevelEditorModule = FModuleManager::LoadModuleChecked<FLevelEditorModule>("LevelEditor");

	// Sets up OnWorldCreated delegate
	FWorldDelegates::FWorldInitializationEvent::FDelegate OnWorldCreatedDelegate;
	OnWorldCreatedDelegate = FWorldDelegates::FWorldInitializationEvent::FDelegate::CreateRaw(this, &FShotgunPlugModule::OnWorldCreated);
	FDelegateHandle OnWorldCreatedDelegateHandle = FWorldDelegates::OnPostWorldInitialization.Add(OnWorldCreatedDelegate);

	// Extends the viewport right-click menu with the plugin functionality
	FLevelEditorModule::FLevelViewportMenuExtender_SelectedActors LevelViewportContextMenuPluginExtender; 

	// Sets up AddContextMenuExtension delegate
	LevelViewportContextMenuPluginExtender = FLevelEditorModule::FLevelViewportMenuExtender_SelectedActors::CreateRaw(this, &FShotgunPlugModule::AddContextMenuExtension);
	auto& MenuExtenders = LevelEditorModule.GetAllLevelViewportContextMenuExtenders();

	// Adds to extender
	MenuExtenders.Add(LevelViewportContextMenuPluginExtender);

	// Handle for ShutdownModule() cleanup
	MenuExtenderDelegateHandle = MenuExtenders.Last().GetHandle();
}


void FShotgunPlugModule::ShutdownModule()
{
	/* This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	*  we call this function before unloading the module.
	*/
	FShotgunPlugStyle::Shutdown();
	// Remove level viewport context menu extenders
	if (FModuleManager::Get().IsModuleLoaded("LevelEditor"))
	{
		FLevelEditorModule& LevelEditorModule = FModuleManager::LoadModuleChecked<FLevelEditorModule>("LevelEditor");
		LevelEditorModule.GetAllLevelViewportContextMenuExtenders().RemoveAll([&](const FLevelEditorModule::FLevelViewportMenuExtender_SelectedActors& Delegate)
		{
			return Delegate.GetHandle() == MenuExtenderDelegateHandle;
		});
	}

	FShotgunPlugCommands::Unregister();
}


void FShotgunPlugModule::OnWorldCreated(UWorld* World, const UWorld::InitializationValues IVS) 
{
	// Live-load all Shotgun users, store them in the Repo
	Connection.LoadAllUsersForProject(GRAVITY_PROJECT_ID);
	// Live-load all task templates, store them in the Repo
	Connection.LoadAllTaskTemplatesForProject(GRAVITY_PROJECT_ID);

	// Find all the actors in the world already tracked by Shotgun 
	TArray<AActor*> Tracked = GetAllShotgunTrackedActors(World);
	for (AActor* elem : Tracked) 
	{
		QuickLog(elem->GetName()); // Debug
		// Access the ShotgunData component to get Shotgun ID
		UShotgunData* TrackedID = elem->FindComponentByClass<UShotgunData>();

		// Get all the tasks for this asset.
		// The first call will populate the Repo via live-load from Shotgun, later calls will access the Repo model only
		// until we determine the data is out of sync or manually refresh.
		std::vector<std::shared_ptr<ShotgunTask>> Tasks = Connection.GetAllTasksForAsset(TrackedID->ShotgunID);

		for (std::shared_ptr<ShotgunTask> Task : Tasks) 
		{
			QuickLog(FString(Task->GetName().c_str())); // Debug
		}
	}
}


TArray<AActor*> FShotgunPlugModule::GetAllShotgunTrackedActors(UWorld* World) 
{
	TArray<AActor*> Tracked;
	for (TActorIterator<AActor> ActorItr(World); ActorItr; ++ActorItr)
	{
		AActor* Actor = *ActorItr;
		// Actors currently tracked by Shotgun will have a UShotgunData component containing their unique Shotgun ID
		if (Actor->FindComponentByClass<UShotgunData>())
		{
			Tracked.Add(Actor);
		}
	}
	return Tracked;
}

TSharedRef<FExtender> FShotgunPlugModule::AddContextMenuExtension(const TSharedRef<FUICommandList> CommandList, TArray<AActor*> SelectedActors) 
{
	// Map the in-editor test actions Swap and Invert. These are placeholders for future development.
	FTimerDelegate SwapDelegate = FExecuteAction::CreateRaw(this, &FShotgunPlugModule::SwapTwoActorsPosition, SelectedActors);
	CommandList->MapAction(FShotgunPlugCommands::Get().SwapActorsLocationCommandInfo, SwapDelegate, FCanExecuteAction());
	FTimerDelegate InvertDelegate = FExecuteAction::CreateRaw(this, &FShotgunPlugModule::InvertActor, SelectedActors);
	CommandList->MapAction(FShotgunPlugCommands::Get().InvertActorCommandInfo, InvertDelegate, FCanExecuteAction());

	// Menu extender for right-click
	TSharedPtr<FExtender> Extender = MakeShareable(new FExtender);
	// This delegate gives us the MenuBuilder which will be passed to FillPluginMenuOptions()
	FMenuExtensionDelegate MenuExtensionDelegate = FMenuExtensionDelegate::CreateRaw(this, &FShotgunPlugModule::FillPluginMenuOptions, SelectedActors);

	// Add to existing context menu using extension hook
	Extender->AddMenuExtension("ActorAsset", EExtensionHook::After, CommandList, MenuExtensionDelegate);
	return Extender.ToSharedRef();
}





void FShotgunPlugModule::FillPluginMenuOptions(FMenuBuilder& MenuBuilder, TArray<AActor*> SelectedActors)
{
	if (0 == SelectedActors.Num()) return;

	MenuBuilder.BeginSection(NAME_None, LOCTEXT("MyplugsHeading", "Pipeline"));
	// Populate the MenuBuilder with the commands appropriate to the number of actors selected
	if (2 == SelectedActors.Num()) 
	{
		// Swap test-command
		MenuBuilder.AddMenuEntry(FShotgunPlugCommands::Get().SwapActorsLocationCommandInfo);
	}

	if (1 == SelectedActors.Num()) 
	{
		// Invert test-command
		MenuBuilder.AddMenuEntry(FShotgunPlugCommands::Get().InvertActorCommandInfo);

		// Shotgun menu options
		AActor* SelectedActor = SelectedActors[0];
		// Shotgun-tracked assets will have a UShotgunData component
		UShotgunData* ShotgunDataActorComponent = SelectedActor->FindComponentByClass<UShotgunData>();

		if (ShotgunDataActorComponent) 
		{
			int EditorAssetShotgunID = ShotgunDataActorComponent->ShotgunID;
			std::string EditorActorShotgunName = TCHAR_TO_UTF8(*ShotgunDataActorComponent->ShotgunName.ToString()); // For debug
			// Check the actor's UShotgunData component has an ID stored in repo 
			if (Connection.IsInRepo(EditorAssetShotgunID)) 
			{
				// This actor is already tracked in Shotgun
				BuildTaskAssignMenus(MenuBuilder, EditorAssetShotgunID);
			}
		}
		else 
		{
			 // Actor not tracked
			 BuildTaskTemplateCreationMenus(MenuBuilder, SelectedActor);
			 
		}
		MenuBuilder.EndSection();
	}
}


TMap<FPipelineStep, TMap<FTaskName, TMap<FUserName, UserTaskPayload>>> FShotgunPlugModule::GetTaskMapsForAsset(int AssetID) 
{
	std::vector<std::shared_ptr<ShotgunUser>> AssignableUsers = Connection.GetCachedUsers();
	// If data model is in-sync (default behavior for prototype) these will already be cached in the Repo at plugin startup
	std::vector<std::shared_ptr<ShotgunTask>> Tasks = Connection.GetAllTasksForAsset(AssetID);

	// Organise the tasks so as to be selectable by  PipelineStep -> TaskName -> Username 
	// Each layer of the nested maps unfolds to a submenu
	TMap<FPipelineStep, TMap<FTaskName, TMap<FUserName, UserTaskPayload>>> CascadingMenuMaps;
	for (std::shared_ptr<ShotgunTask> Task : Tasks)
	{
		// TODO currently we allow all users to be assigned to any task. Refactor to filter by user department.
		for (std::shared_ptr<ShotgunUser> Assignee : AssignableUsers)
		{
			FPipelineStep PipelineStep = FString(Task->GetPipelineStep().c_str());
			FTaskName TaskName = FString(Task->GetName().c_str());
			FUserName UserName = FString(Assignee->GetName().c_str());

			// The payload is passed through the menu sublevels to the end of the cascade where the user triggers the Shotgun task-assign
			UserTaskPayload Payload = { Assignee, Task };
			// Populates the nested maps, drilling down into each level or creating if not present
			CascadingMenuMaps.FindOrAdd(PipelineStep).FindOrAdd(TaskName).Add(UserName, Payload);
		}
	}
	return CascadingMenuMaps;
}

// Task-assign menu-build: step 0
void FShotgunPlugModule::BuildTaskAssignMenus(FMenuBuilder& MenuBuilder, int EditorAssetShotgunID) 
{
	TMap<FPipelineStep, TMap<FTaskName, TMap<FUserName, UserTaskPayload>>> CascadingMenuMaps = GetTaskMapsForAsset(EditorAssetShotgunID);
	if (0 != CascadingMenuMaps.Num()) 
	{
		// Create submenu builder delegate: FillPipelineStepSubMenu() 
		// Automatically binds the MenuBuilder to arg0 and manually binds arg1 to CascadingMenuMaps
		FMenuExtensionDelegate FillPipelineStepMenuDelegate = FNewMenuDelegate::CreateRaw(this, &FShotgunPlugModule::FillPipelineStepSubMenu, CascadingMenuMaps);
		MenuBuilder.AddSubMenu(
			LOCTEXT("ShotgunMenu", "Assign Shotgun Task"),
			LOCTEXT("ShotgunMenu_ToolTip", ""),
			FillPipelineStepMenuDelegate);
	}

}


// Task-assign menu-build: step 1
void FShotgunPlugModule::FillPipelineStepSubMenu(FMenuBuilder& MenuBuilder,
	TMap<FPipelineStep, TMap<FTaskName, TMap<FUserName, UserTaskPayload>>> CascadingMenuMaps)
{
	for (TPair<FPipelineStep, TMap<FTaskName, TMap<FUserName, UserTaskPayload>>> Entry : CascadingMenuMaps) 
	{
		// Pipeline steps are the first set of keys as the nested maps are unpacked 
		FPipelineStep PipelineStep = Entry.Key;
		TMap<FTaskName, TMap<FUserName, UserTaskPayload>> TaskNamesMap = Entry.Value;

		// Bind/create next submenu builder delegate
		FMenuExtensionDelegate FillTaskNameSubMenuDelegate = FNewMenuDelegate::CreateRaw(this, &FShotgunPlugModule::FillTaskNameSubMenu, TaskNamesMap);
		const FText ToolTipName = LOCTEXT("FillPipelineStepSubMenu_ToolTip", "Pipeline Step");

		MenuBuilder.AddSubMenu(FText::FromString(PipelineStep), ToolTipName, FillTaskNameSubMenuDelegate);
	}

}


// Task-assign menu-build: step 2
void FShotgunPlugModule::FillTaskNameSubMenu(FMenuBuilder& MenuBuilder,
	TMap<FTaskName, TMap<FUserName, UserTaskPayload>> TaskNamesMap)
{
	for (TPair<FTaskName, TMap<FUserName, UserTaskPayload>> Entry : TaskNamesMap)
	{
		// Task names are the next set of keys as the nested maps are unpacked
		FTaskName TaskName = Entry.Key;
		TMap<FUserName, UserTaskPayload> UsernameTaskMap = Entry.Value;

		// Bind/create next submenu builder delegate
		FNewMenuDelegate FillUserNameSubMenuDelegate = FNewMenuDelegate::CreateRaw(this, &FShotgunPlugModule::FillUserNameSubMenu, UsernameTaskMap);
		const FText ToolTipName = LOCTEXT("TaskSubmenu_Tooltip", "Task");

		MenuBuilder.AddSubMenu(FText::FromString(TaskName), ToolTipName, FillUserNameSubMenuDelegate);
	}
}


// Task-assign menu-build: step 3
void FShotgunPlugModule::FillUserNameSubMenu(class FMenuBuilder& MenuBuilder, TMap<FUserName, UserTaskPayload> UsernameTaskMap) 
{
	for (TPair<FUserName, UserTaskPayload> elem : UsernameTaskMap)  
	{
		// Shotgun Usernames are the last set of keys as the nested maps are unpacked
		FUserName UserName = elem.Key;

		// The Payload holds shared_ptrs to the ShotgunUser and the original ShotgunTask that we've now grouped and viewed by PipelineStep->TaskName->AssignableUser.
		// They holds the ID's needed to do the task assign through the Python API.
		UserTaskPayload Payload = elem.Value;
		const FText ToolTipName = LOCTEXT("AssignTaskToSubmenu_Tooltip", "Assign this task to");

		// We don't want to create a new action every time this point is reached so add to map for retrieval.
		// Map key will be a combination of the asset id, user and task names.
		std::stringstream Concatenate;
		Concatenate << Payload.Task->GetAsset()->GetID() << Payload.Assignee->GetName() << Payload.Task->GetName();
		FString Key = FString(Concatenate.str().c_str());

		TSharedRef<FUIAction>* PreviouslyAddedAction = AssignTaskActionLookup.Find(Key);
		if (!PreviouslyAddedAction)
		{
			TSharedRef<FUIAction> NewAction = MakeShared<FUIAction>();

			// Map the user-click to an FUIAction, binding to ShotgunUserTaskAssign with arg0 as Payload
			NewAction->ExecuteAction.BindRaw(this, &FShotgunPlugModule::ShotgunUserTaskAssign, Payload);
			// Store the action for later retreival 
			AssignTaskActionLookup.Add(Key, NewAction);

			MenuBuilder.AddMenuEntry(FText::FromString(UserName), ToolTipName, FSlateIcon(), *NewAction);
		}
		else
		{
			// Retrieve from cached FUIActions
			MenuBuilder.AddMenuEntry(FText::FromString(UserName), ToolTipName, FSlateIcon(), **PreviouslyAddedAction);
		}


	}
}


// Asset-create menu-build: step 0
void FShotgunPlugModule::BuildTaskTemplateCreationMenus( FMenuBuilder& MenuBuilder, AActor* SelectedActor) 
{
	
	// Create submenu builder delegate: FillTaskTemplateSubMenu
	// Automatically binds the MenuBuilder to arg0 and manually binds arg1 to SelectedActor
	FMenuExtensionDelegate FillTaskTemplateCreationMenuDelegate = FNewMenuDelegate::CreateRaw(this, &FShotgunPlugModule::FillTaskTemplateSubMenu, SelectedActor);
	MenuBuilder.AddSubMenu(
		LOCTEXT("ShotgunTaskTemplateMenu", "Create Shotgun Asset"),
		LOCTEXT("ShotgunTaskTemplateMenu_Tooltip", "Shotgun Task Templates"),
		FillTaskTemplateCreationMenuDelegate);
}


// Asset-create menu-build: step 1
void FShotgunPlugModule::FillTaskTemplateSubMenu(FMenuBuilder& MenuBuilder, AActor* SelectedActor) 
{
	std::vector<std::shared_ptr<ShotgunTaskTemplate>> ShotgunTaskTemplates = Connection.GetCachedTaskTemplates();
	for (std::shared_ptr<ShotgunTaskTemplate> TaskTemplate : ShotgunTaskTemplates) 
	{
		const FText ToolTipName = LOCTEXT("ShotgunTaskTemplateSubmenu_Tooltip:", "Create Shotgun asset with"); 
		FString TaskTemplateName = FString(TaskTemplate->GetName().c_str());

		// We don't want to create a new action every time this point is reached so add to map for retrieval
		// Map key will be a combination of the Task Template and Actor names
		FString Key = SelectedActor->GetName().Append(FString(TaskTemplate->GetName().c_str()));

		TSharedRef<FUIAction>* PreviouslyAddedAction = TaskTemplateActionLookup.Find(Key);
		if (!PreviouslyAddedAction) 
		{ // Prevents duplication of FUIAction creation
			TSharedRef<FUIAction> NewAction = MakeShared<FUIAction>();
			TaskTemplateActionLookup.Add(TaskTemplateName, NewAction);

			// Map the user-click to an FUIAction, binding to ShotgunTaskTemplateCreation() with arg0 as TaskTemplate and arg1 as SelectedActor
			NewAction->ExecuteAction.BindRaw(this, &FShotgunPlugModule::ShotgunTaskTemplateCreation, TaskTemplate, SelectedActor);
			MenuBuilder.AddMenuEntry(FText::FromString(TaskTemplateName), ToolTipName, FSlateIcon(), *NewAction);

		}
		else 
		{
			MenuBuilder.AddMenuEntry(FText::FromString(TaskTemplateName), ToolTipName, FSlateIcon(), **PreviouslyAddedAction);
		}

	}

}



void FShotgunPlugModule::ShotgunUserTaskAssign(UserTaskPayload Payload)
{
	// TODO we assume this succeeds for now, extend code with validation and fail-safe  
	Connection.AssignTaskToUser(Payload.Task->GetID(), Payload.Assignee->GetID());
	UE_LOG(LogTemp, Warning, TEXT("%s assigned to %s"), ANSI_TO_TCHAR(Payload.Assignee->GetName().c_str()), ANSI_TO_TCHAR(Payload.Task->GetName().c_str()))
	UE_LOG(LogTemp, Warning, TEXT("%d assigned to %d"), Payload.Task->GetID(), Payload.Assignee->GetID())
}


void FShotgunPlugModule::ShotgunTaskTemplateCreation(std::shared_ptr<ShotgunTaskTemplate> TaskTemplate, AActor* SelectedActor) 
{
	QuickLog(ANSI_TO_TCHAR(TaskTemplate->GetName().c_str()));
	std::string ShotgunName =  TCHAR_TO_UTF8(*SelectedActor->GetName());

	// Make the Shotgun call and load the returned ID. This call automatically loads the newly created tasks and asset into the Connection's Repo
	int NewTrackedAssetID = Connection.CreateAssetWithTaskTemplate(ShotgunName, TaskTemplate->GetID());

	// Create ShotgunDataComponent to add to actor. This will store the returned ID for future tracking and task-assign
	QuickLog(TEXT("Creating ShotgunData component"));
	UShotgunData* NewShotgunDataComponent = NewObject<UShotgunData>(SelectedActor, FName("ShotgunData")); 

	NewShotgunDataComponent->RegisterComponent();
	NewShotgunDataComponent->ShotgunID = NewTrackedAssetID;
	NewShotgunDataComponent->ShotgunName = FText::FromString(SelectedActor->GetName());
	SelectedActor->AddInstanceComponent(NewShotgunDataComponent);
}



void FShotgunPlugModule::InvertActor(TArray<AActor*> SelectedActors) 
{
	if (GEditor && 1 == SelectedActors.Num()) 
	{
		// FillPluginOptions() has bound this function to the case where there is exactly one actor selected
		AActor* SingleSelectedActor = SelectedActors[0];
		if (SingleSelectedActor)
		{
			// Start transaction for Undo 
			GEditor->BeginTransaction(LOCTEXT("Invert", "Invert Actor")); // Appears in the Edit menu as undo option
			// Snapshot for Undo-rollback
			SingleSelectedActor->GetRootComponent()->Modify();

			FVector NewInvertedScale = FVector(
				SingleSelectedActor->GetActorScale().X,
				SingleSelectedActor->GetActorScale().Y,
				SingleSelectedActor->GetActorScale().Z*-1);
			// NB Actor will need to be Moveable in the editor for the shadows to update
			SingleSelectedActor->SetActorScale3D(NewInvertedScale);

			// End transaction for Undo 
			GEditor->EndTransaction();
		}
	}
}

void FShotgunPlugModule::SwapTwoActorsPosition(TArray<AActor*> SelectedActors) 
{
	// FillPluginOptions() has bound this function to the case where there are exactly two actors selected
	if (GEditor && 2 == SelectedActors.Num())
	{
		AActor* Actor1 = SelectedActors[0];
		AActor* Actor2 = SelectedActors[1];
		if (Actor1 && Actor2)
		{
			// Start transaction for Undo 
			GEditor->BeginTransaction(LOCTEXT("Swap", "Swap Actors")); // Appears in the Edit menu as undo option
			// Snapshot for Undo-rollback
			Actor1->GetRootComponent()->Modify();
			Actor2->GetRootComponent()->Modify();

			FVector Temp = Actor1->GetActorLocation();
			// NB Actors will need to be Moveable in the editor for the shadows to update
			Actor1->SetActorLocation(Actor2->GetActorLocation());
			Actor2->SetActorLocation(Temp);

			// End transaction for Undo 
			GEditor->EndTransaction();
		}
	}
}



#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FShotgunPlugModule, ShotgunPlug)