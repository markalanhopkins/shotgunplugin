// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "ShotgunPlugPrivatePCH.h"
#include "ShotgunPlugCommands.h"


#define LOCTEXT_NAMESPACE "FShotgunPlugModule"

void FShotgunPlugCommands::RegisterCommands()
{
	UI_COMMAND(SwapActorsLocationCommandInfo, "Swap", "Swap Actors Location", EUserInterfaceActionType::Button, FInputGesture());
	UI_COMMAND(InvertActorCommandInfo, "Invert", "Invert Actor", EUserInterfaceActionType::Button, FInputGesture());

}

#undef LOCTEXT_NAMESPACE
