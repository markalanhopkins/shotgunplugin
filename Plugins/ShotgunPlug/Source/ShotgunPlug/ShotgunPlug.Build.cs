// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

// Arbitary commit Recovery from rename problem

public class ShotgunPlug : ModuleRules
{
    public ShotgunPlug(TargetInfo Target)
    {

        PublicIncludePaths.AddRange(
            new string[] {
                "ShotgunPlug/Public",
                "C:/Users/Mark Hopkins/Code/cpp/Shotgun/ShotgunTools/ShotgunTools"
				// ... add public include paths required here ...
			}
            );


        PrivateIncludePaths.AddRange(
            new string[] {
                "ShotgunPlug/Private",
				// ... add other private include paths required here ...
			}
            );

        // Add static libs
        PublicAdditionalLibraries.Add("C:/Users/Mark Hopkins/Code/cpp/Shotgun/ShotgunTools/x64/Release/ShotgunTools.lib");


        PublicDependencyModuleNames.AddRange(
            new string[]
            {
                "Core",
				// ... add other public dependencies that you statically link with here ...
			}
            );


        PrivateDependencyModuleNames.AddRange(
            new string[]
            {
                "Projects",
                "InputCore",
                "UnrealEd",
                "LevelEditor",
                "CoreUObject",
                "Engine",
                "Slate",
                "SlateCore",
				// ... add private dependencies that you statically link with here ...	
			}
            );


        DynamicallyLoadedModuleNames.AddRange(
            new string[]
            {
				// ... add any modules that your module loads dynamically here ...
			}
            );
    }
}
